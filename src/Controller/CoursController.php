<?php

namespace App\Controller;

use App\Entity\Cours;
use App\Repository\CoursRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoursController extends AbstractController
{
    /**
     * @Route("/cours", name="cours")
     */
    public function index(CoursRepository $coursRepo)
    {

        $cours = $coursRepo->findAll();

        return $this->render('cours/showCours.html.twig', [
            "cours" => $cours
        ]);
    }

    /**
     * @Route("/cour/show/{id}", name="cour")
     */
    public function show(Cours $cour){

        return $this->render('cours/cour.html.twig',[
            'cour' => $cour
        ]);
    }

    /**
     * @Route("/cour/delete/{id}", name="cours_delete")
     */
    public function delete(Cours $cour): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($cour);
        $entityManager->flush();

        //TODO revoir la condition de suppression d'un cours

        return $this->redirectToRoute('cours');
    }

}
