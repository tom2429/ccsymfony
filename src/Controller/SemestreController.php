<?php

namespace App\Controller;

use App\Entity\Cours;
use App\Entity\Semestre;
use App\Repository\CoursRepository;
use App\Repository\SemestreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SemestreController extends AbstractController
{
    /**
     * @Route("/semestre", name="semestre")
     */
    public function index(SemestreRepository $semestreRepo)
    {

        $semestres  = $semestreRepo->findAll();

        return $this->render('semestre/showSemestre.html.twig', [
            'semestres' => $semestres
        ]);
    }

    /**
     * @Route("/semetre/{id}", name="semestre_cour")
     */
    public function show_cour(Semestre $semestre){

        return $this->render("semestre/showSemestreCour.html.twig",[
            "semestre" => $semestre
        ]);
    }

    /**
     * @Route("/semestre/delete/{id}", name="semestre_delete")
     */
    public function delete(Semestre $semestre): Response
    {
        foreach ($semestre->getCours() as $cours ){
            $cours->setSemestre(null);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($semestre);
        $entityManager->flush();

        //TODO revoir la condition de suppression d'un cours

        return $this->redirectToRoute('semestre');
    }
}
