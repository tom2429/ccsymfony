<?php

namespace App\DataFixtures;

use App\Entity\Cours;
use App\Entity\Semestre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Faker\Factory as Faker;

class DataFixtures extends Fixture
{
    public function load($manager)
    {
        $faker = Faker::create('fr_FR');

        $semestre = new Semestre();
        $semestre->setNomFormation("Informatique IMIS")
                 ->setNomSemestre("S5");
        $manager->persist($semestre);

        for ($i = 0; $i < 10; $i++) {
            $cour = new Cours();
            $cour->setNom($faker->name)
                 ->setDescription($faker->text)
                 ->setSemestre($semestre);
            $manager->persist($cour);
        }

        $manager->flush();
    }
}
